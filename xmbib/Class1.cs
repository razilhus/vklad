﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmbib
{
    public class Prog

    {
        private double _summa;
        private double _procent;
        private int _srok;


        public Prog (double _summa, double _procent, int _srok)
        {
            this._summa = _summa;
            this._procent = _procent;
            this._srok = _srok;
        }

        public double[] raschet()
        {
            double[] array = new double[_srok];

            array[0] = _summa;
            
            for (int i = 1; i < _srok; i++)
            {
                double r = _summa * (_procent / _srok);
                array[i] = array[i-1] + r;
                array[i] = _summa + r*(i + 1);
            }
            return array;
        }
    }
}






